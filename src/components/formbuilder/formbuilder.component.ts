import {
  Component,
  Input,
  OnInit,
  OnChanges,
  OnDestroy,
  ViewEncapsulation,
  Optional,
  ElementRef,
  ViewChild, EventEmitter, Output
} from '@angular/core';
import { FormioAppConfig } from '../../formio.config';
import {
  FormioForm,
  FormioOptions,
  FormioRefreshValue
} from '../../formio.common';
import { Formio, FormBuilder, Utils } from 'formiojs';
import { assign, isEqual } from 'lodash';

/* tslint:disable */
@Component({
  selector: 'form-builder',
  templateUrl: './formbuilder.component.html',
  styleUrls: ['./formbuilder.component.scss'],
  encapsulation: ViewEncapsulation.None
})
/* tslint:enable */
export class FormBuilderComponent implements OnInit, OnChanges, OnDestroy {
  public _ready: Promise<object>;
  public readyResolve: any;
  public formio: any;
  public builder: FormBuilder;
  public componentAdding = false;
  @Input() form?: FormioForm;
  @Input() options?: FormioOptions;
  @Input() formbuilder?: any;
  @Input() refresh?: EventEmitter<FormioRefreshValue>;
  @Input() noeval ? = false;
  @Output() change?: EventEmitter<object>;
  @Output() ready?: EventEmitter<object>;
  @ViewChild('builder', { static: true }) builderElement?: ElementRef<any>;

  constructor(
    @Optional() private config: FormioAppConfig
  ) {
    if (this.config) {
      Formio.setBaseUrl(this.config.apiUrl);
      Formio.setProjectUrl(this.config.appUrl);
    } else {
      console.warn('You must provide an AppConfig within your application!');
    }

    this.change = new EventEmitter();
    this.ready = new EventEmitter();
    this._ready = new Promise((resolve: any) => {
      this.readyResolve = resolve;
    });
  }

  ngOnInit() {
    if (this.refresh) {
      this.refresh.subscribe((refresh: FormioRefreshValue) =>
        this.onRefresh(refresh)
      );
    }
    Utils.Evaluator.noeval = this.noeval;
  }

  setInstance(instance: any) {
    if (this.formio) {
      this.formio.off('addComponent');
      this.formio.off('saveComponent');
      this.formio.off('pasteComponent');
      this.formio.off('saveElement');
      this.formio.off('updateComponent');
      this.formio.off('deleteComponentForMoving');
      this.formio.off('deleteComponent');
      this.formio.off('validationErrors');
      this.formio.off('notify');
    }
    this.formio = instance;
    instance.off('addComponent');
    instance.off('saveComponent');
    instance.off('pasteComponent');
    instance.off('saveElement');
    instance.off('updateComponent');
    instance.off('deleteComponent');
    instance.off('validationErrors');
    instance.off('deleteComponentForMoving');
    instance.off('notify');
    instance.on('addComponent', (event) => {
      if (event.isNew) {
        this.componentAdding = true;
      } else {
        this.change.emit({
          type: 'addComponent',
          builder: instance,
          form: instance.schema,
          component: event
        });
        this.componentAdding = false;
      }
    });
    instance.on('saveComponent', (event, originalComponent) => {
      if (this.formio === instance) {
        this.change.emit({
          type: this.componentAdding ? 'addComponent' : 'saveComponent',
          builder: instance,
          form: instance.schema,
          component: event,
          originalComponent
        });
        this.componentAdding = false;
      }
    });
    instance.on('pasteComponent', (event) => {
      if (this.formio === instance) {
        this.change.emit({
          type: 'pasteComponent',
          builder: instance,
          form: instance.schema,
          component: event
        });
      }
    });
    instance.on('saveElement', (event) => {
      if (this.formio === instance) {
        this.change.emit({
          type: 'saveElement',
          builder: instance,
          form: instance.schema,
          component: event
        });
        this.componentAdding = false;
      }
    });
    instance.on('updateComponent', (event) => {
      if (this.formio === instance) {
        this.change.emit({
          type: 'updateComponent',
          builder: instance,
          form: instance.schema,
          component: event
        });
      }
    });
    instance.on('deleteComponentForMoving', (event) => {
      if (this.formio === instance) {
        this.change.emit({
          type: 'deleteComponentForMoving',
          builder: instance,
          form: instance.schema,
          component: event
        });
      }
    });
    instance.on('deleteComponent', (event) => {
      if (this.formio === instance) {
        this.change.emit({
          type: 'deleteComponent',
          builder: instance,
          form: instance.schema,
          component: event
        });
      }
    });
    instance.on('validationErrors', (errors, component) => {
      if (this.formio === instance) {
        this.change.emit({
          type: 'validationErrors',
          builder: instance,
          form: instance.schema,
          errors,
          component
        });
      }
    });
    instance.on('notify', (message, component) => {
      if (this.formio === instance) {
        this.change.emit({
          type: 'notify',
          builder: instance,
          form: instance.schema,
          message,
          component
        });
      }
    });
    this.readyResolve(instance);
    this.ready.emit({instance});
    return instance;
  }

  setDisplay(display: String) {
    return this.builder.setDisplay(display).then(instance => this.setInstance(instance));
  }

  buildForm(form) {
    if (!form || !this.builderElement || !this.builderElement.nativeElement) {
      return;
    }

    if (this.builder) {
      return this.setDisplay(form.display).then(() => {
        this.builder.form = form;
        this.builder.instance.form = form;
        return this.builder.instance;
      });
    }
    const Builder = this.formbuilder || FormBuilder;
    this.builder = new Builder(
      this.builderElement.nativeElement,
      form,
      assign({icons: 'fontawesome'}, this.options || {})
    );
    return this.builder.render().then(instance => this.setInstance(instance));
  }

  processTabRefresh(refresh: FormioRefreshValue, changed: boolean): boolean {
    if (Array.isArray(refresh.submission)) {
      const tabs = [];
      const currentTabComponent = this.formio.editForm.data.components[this.formio.editForm.editComponent.currentTab];
      refresh.submission.forEach((tab, tabIndex) => {
        let existing = this.formio.editForm.data.components.find(c => c.id && c.id === tab.id);
        if (!existing) {
          // WTF: why bpa-be generated new id?
          existing = this.formio.editForm.data.components.find(c => c.key && c.key === tab.key);
        }
        if (existing) {
          for (const key of ['id', 'label', 'hidden', 'key', 'condition', 'group', 'workingLabel', 'icon', 'titleMode', 'iconPosition']) {
            if (existing[key] !== tab[key]) {
              existing[key] = tab[key];
              changed = true;
            }
          }
        }
        const eachTab = existing || tab;
        if (Array.isArray(eachTab.components)) {
          eachTab.components.forEach(c => c.tab = tabIndex);
        } else {
          eachTab.components = [];
        }
        tabs.push(eachTab);
      });
      changed = changed || !isEqual(this.formio.editForm.data.components, tabs);
      this.formio.editForm.data.components = tabs;
      if (currentTabComponent) {
        let currentTab = this.formio.editForm.data.components.find(c => currentTabComponent.id && currentTabComponent.id === c.id);
        if (!currentTab) {
          currentTab = this.formio.editForm.data.components.find(c => currentTabComponent.key && currentTabComponent.key === c.key);
        }
        this.formio.editForm.editComponent.currentTab = currentTab ? this.formio.editForm.data.components.indexOf(currentTab) : 0;
      }
    }
    return changed;
  }

  onRefresh(refresh: FormioRefreshValue) {
    if (!refresh.form || !this.formio || !this.formio.editForm || this.formio.editForm.data.componentJson) {
      return;
    }
    console.log('refresh message: ', JSON.stringify(refresh));
    const formType = refresh.form.toString();
    let changed = false;
    let count = refresh.value;
    switch (formType) {
      case 'receiveCurrentComponent':
        this.change.emit({'type': 'receivedCurrentComponent', data: this.formio.editForm.data});
        return;
      case 'relationChange':
        const changeObj = refresh.value;
        changed = !Utils.isSubset(this.formio.editForm.data, changeObj['changes']) || changeObj['changes'].registrations;
        Object.assign(this.formio.editForm.data, changeObj['changes']);
        if (changeObj['changes'].componentTabId) {
          changed = this.processTabRefresh(refresh, changed);
        }
        count = changeObj['count'];
        break;
      case 'form':
        changed = !isEqual(this.formio.editForm.data.conditional, count);
        this.formio.editForm.data.conditional = count;
        break;
      case 'formula':
        changed = !isEqual(this.formio.editForm.data.calculateValue, count);
        this.formio.editForm.data.calculateValue = count;
        break;
      case 'numberOfActions':
        changed = this.formio.editForm.data.numberOfActions !== count;
        this.formio.editForm.data.numberOfActions = count;
        break;
      case 'numberOfConditions':
        changed = this.formio.editForm.data.numberOfConditions !== count;
        this.formio.editForm.data.numberOfConditions = count;
        break;
      case 'numberOfFormulas':
        changed = this.formio.editForm.data.numberOfFormulas !== count;
        this.formio.editForm.data.numberOfFormulas = count;
        break;
      case 'numberOfRegistrations':
        changed = typeof this.formio.editForm.data.numberOfRegistrations !== 'undefined'
          && !isEqual(this.formio.editForm.data.numberOfRegistrations, count);
        this.formio.editForm.data.numberOfRegistrations = count;
        break;
      case 'copyValueFromDeterminant':
        this.formio.editForm.data.copyValueFromDeterminant = count;
        break;
      case 'dynamicValidations':
        this.formio.editForm.data.dynamicValidations = count;
        break;
      case 'numberOfRelatedDeterminants':
        changed = this.formio.editForm.data.numberOfRelatedDeterminants !== count;
        this.formio.editForm.data.numberOfRelatedDeterminants = count;
        break;
      case 'numberOfTabs':
        changed = !isEqual(this.formio.editForm.data.numberOfTabs, count);
        this.formio.editForm.data.numberOfTabs = count;
        changed = this.processTabRefresh(refresh, changed);
        break;
    }
    if (changed && !this.formio.editForm.editComponent.isNew) {
      for (const key of Object.keys(this.formio.editForm.data)) {
        this.formio.editForm.editComponent.component[key] = this.formio.editForm.data[key];
      }
      const oldElement = this.formio.editForm.editComponent.element;
      if (oldElement && oldElement.parentNode) {
        this.formio.editForm.editComponent.redraw(true);
        oldElement.parentNode.replaceChild(this.formio.editForm.editComponent.element, oldElement);
        // tslint:disable-next-line:max-line-length
        this.formio.options.hooks.addComponent(this.formio.editForm.editComponent.parent.element, this.formio.editForm.editComponent, this.formio.editForm.editComponent.parent);
      }
      this.formio.emit('saveElement', this.formio.editForm.data);
      this.formio.emit('saveComponent', this.formio.editForm.editComponent, {});
    }

    this.formio.editForm.components.filter(c => c.component && c.component.type === 'tabs').forEach(tab => {
      let tabLinkHref = null;
      switch (refresh.form.toString()) {
        case 'relationChange':
          if (refresh.value['changes'].formDeterminantId) {
            tabLinkHref = '#determinant';
          } else if (refresh.value['changes'].effectsIds) {
            tabLinkHref = '#determinant';
          } else if (refresh.value['changes'].componentFormulaId) {
            tabLinkHref = '#formula';
          } else if (refresh.value['changes'].componentActionId) {
            tabLinkHref = '#action';
          } else if (refresh.value['changes'].componentTabId) {
            tabLinkHref = '#tabTabs';
          } else if (refresh.value['changes'].registrations) {
            tabLinkHref = '#registration';
          }
          break;
        case 'numberOfFormulas':
          tabLinkHref = '#formula';
          break;
        case 'numberOfActions':
          tabLinkHref = '#action';
          break;
        case 'numberOfConditions':
          tabLinkHref = '#determinant';
          break;
        case 'numberOfRelatedDeterminants':
          tabLinkHref = '#relatedDeterminant';
          break;
        case 'numberOfRegistrations':
          tabLinkHref = '#registration';
          break;
        case 'numberOfTabs':
          tabLinkHref = '#tabTabs';
          break;
      }
      let tabLink = tab.tabLinks.find(link => link.lastElementChild.getAttribute('href') === tabLinkHref);
      if (!tabLink) { return; }
      tabLink.lastElementChild.innerHTML = tabLink.lastElementChild.innerHTML.replace(/\(\d*\)/g,  `(${count})`);
    });
  }

  ngOnChanges(changes: any) {
    if (changes.form && changes.form.currentValue) {
      this.buildForm(changes.form.currentValue || {components: []});
    }
  }

  ngOnDestroy() {
    if (this.formio) {
      this.formio.destroy();
    }
  }
}
