import { OnInit, OnChanges, OnDestroy, ElementRef, EventEmitter } from '@angular/core';
import { FormioAppConfig } from '../../formio.config';
import { FormioForm, FormioOptions, FormioRefreshValue } from '../../formio.common';
import { FormBuilder } from 'formiojs';
export declare class FormBuilderComponent implements OnInit, OnChanges, OnDestroy {
    private config;
    _ready: Promise<object>;
    readyResolve: any;
    formio: any;
    builder: FormBuilder;
    componentAdding: boolean;
    form?: FormioForm;
    options?: FormioOptions;
    formbuilder?: any;
    refresh?: EventEmitter<FormioRefreshValue>;
    noeval?: boolean;
    change?: EventEmitter<object>;
    ready?: EventEmitter<object>;
    builderElement?: ElementRef<any>;
    constructor(config: FormioAppConfig);
    ngOnInit(): void;
    setInstance(instance: any): any;
    setDisplay(display: String): any;
    buildForm(form: any): any;
    processTabRefresh(refresh: FormioRefreshValue, changed: boolean): boolean;
    onRefresh(refresh: FormioRefreshValue): void;
    ngOnChanges(changes: any): void;
    ngOnDestroy(): void;
}
